'use strict';
const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom')

const init = async () => {
    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route({
        method: 'GET',
        path: '/hello',
        handler: (req, res) => {

            var error = new Error('Deu ruim brother');
            return Boom.boomify(error, { statusCode: 400 });
            // return 'Hello World!';
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
