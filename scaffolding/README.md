<p align="center">
  <img src=".repo/logo.png" alt="logo" width="400" />
</p>

<h1 align="center">scaffolding</h1>

<p align="center">
 <b></b></br>
  <span>Powered by <a href="https://hapijs.com/">Hapi 18</a>  ❤️ </sub>
</p>

# Sumário

* [Requisitos de Instalação](#Requisitos-de-Instalação)
* [Scripts](#Scripts)
* [Documentação](#Documentação)
* [Configuração de Ambiente](#Configuração-de-Ambiente)
* [Debugando a Aplicação](#Debugando-a-Aplicação)
* [Plugins](#Plugins)
* [Extensões Recomendadas](#Extensões-Recomendadas)
* [MySQL](#MySQL)
* [Teresa](#Teresa)
* [Testes](#Testes)
* [Tricks](#Tricks)
* [SonarQube](#SonarQube)

# Requisitos de Instalação

[Visual Studio Code](https://code.visualstudio.com)

> Editor de textos super leve, com depuração e suporte a extensões.

[Node JS 8+](https://nodejs.org/en/)

## Sugestões

> Engine responsável por interpretar nosso Javascript para rodar no server.

[NVM](https://github.com/creationix/nvm)

> Programa responsável para a escolha de versões do node de forma rápida, evita problemas com acesso a pastas com permissões especiais.

[Yarn](https://yarnpkg.com/pt-BR/)

> É um npm melhorado mantido pelo Facebook

---

# Scripts

Todo script é executado via:

```sh
npm run {nome_do_script}
```

A lista completa de scripts você pode encontrar em [package.json](./package.json)

|Nome|Descrição|
|---	|---	|
| build 	|   Efetua a transpoilação do `typescript`	|
| postinstall 	|   O mesmo que o `build`, ele executa após o npm install	|
| start 	|   Inicia a aplicação via TS-NODE	|
| nodemon 	|   Inicia o nodemon para subir em tempo real as alterações no servidor	|
| test 	|   Reseta as migrations, executa migrations e seeders,depois executa os testes via TEST e coleta cobertura	|
| test:debug 	| Reseta as migrations, executa migrations e seeders  	|
| migrate 	| Executa todas as migrations pendentes  	|
| seed 	| Executa todos os seeds 	|
| lint 	| Faz um relatório do seu código, verificando padrões 	|
| lint:fix 	| O mesmo que o `lint` porém já limpa automaticamente 	|

---

 utilize o comando para baixar as dependências do projeto na raiz do seu projeto:

```sh
npm install
```


# Documentação

Utilizamos Swagger em nossa aplicação, você pode acessar utilizando

[http://localhost:9000/documentation](http://localhost:9000/documentation)

Note, que algumas rotas são protegidas, você pode gerar um token usando o `jwtg`
Instale usando:

```sh
npm i -g jwtg
```

Depois execute

```sh
jwtg (palavra chave)
```

Utilize a resposta do comando como paramêtro de autenticação.

---

# Configuração de Ambiente

Nossa aplicação utiliza a lib dotenv, todas as configurações estão disponíveis no diretório ./env
por padrão temos o:
> [env/.env-development](/env/.env-development)

`development` é recebido através da var de ambiente NODE_ENV
caso você alterar, ele irá buscar pela nova palavra.

Configure todas as variáveis dentro desse arquivo.

Para conferir, verifique o arquivo em:
> [src/config/environment.config.ts](/src/config/environment.config.ts)

Acesse a pasta do projeto e configure as variáveis de ambiente de acordo com o ambiente `.env-development`, `.env-stage` e `.env`:

*Exemplo:*

```sh
API_URL=https://myawesome.api
```

*PS.:* Matenha o arquivo `.env-development` sempre atualizado, é nele que nos baseamos para configurar as variáveis de ambiente.

---

# Debugando a aplicação

As configurações de debug estão pré-configuradas no VS Code.

![Visual Studio Code](/.repo/debug.png)


Você pode utilizar o comando

```sh
npm run start
```

# Plugins

Os plugins são configuráveis através de variáveis de ambiente e um arquivo de configuração.

|Nome|Descrição|
|---	|---	|
| Routes 	|  Faz a leitura das routas do projeto	|
| Reply 	|   Toolkit de response da aplicação	|
| Server 	|   Responde via header o nome do server	|
| Version 	|   Responde via header a versão do app	|
| Not Found |   Responde com status 404 caso a rota não é encontrada	|
| Response Time 	| Responde via header o tempo de resposta 	|
| Health Check 	| Implementa duas rotas para validar o status da api	|
| Documentation 	| Habilita o Swagger 	|
| Auth 	| Valida os requests com jwt 	|
| Burzum 	| Salva os logs no burzum |
| Blip 	| Printa todas as rotas da aplicação |
| New Relic 	|  Retorna indicadores da sua aplicação |

## Configurando Plugins

Todas as configurações estão dentro de: [environment.config.ts](./src/config/environment.config.ts)

Todos os plugins se encontram em: [plugins.config.ts](./src/config/plugins.config.ts)

---

## Reply

### `Sucess`

```javascript
return h.success(response, { totalRecords: response.total }).code(OK);
```

### `Sucess com API`

```javascript
return h.successApi(response, { totalRecords: response.total }).code(OK);
```

### `Error`

```javascript
return h.error(error);
```

## Burzum

> Você pode gerar tokens chamando o bot @guedes no canal do slack #burzumlogs

```slack
  @guedes burzum logistica ligeirinho
```

> Após a geração do token e utilizar na aplicação, você deve indexar o índice:
[Kibana - Burzum](https://kibana.burzum.appsluiza.com.br/app/kibana#/management/kibana/indices/magazineluiza-apigee-*?_g=()&_a=(tab:indexedFields))

## Sentry

> A criação de tokens deve ser feita pela seguinte url:

[sentry.appsluiza.com.br](sentry.appsluiza.com.br)
> Configure seus plugins.

# Extensões Recomendadas

## VSCode Extensions

[TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)

[EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)

[GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

[Sort Typescript Imports](https://marketplace.visualstudio.com/items?itemName=miclo.sort-typescript-imports)

# MYSQL

## Setando o horário do banco para UTC

```sql
SET GLOBAL time_zone = '+0:00';
SET time_zone = "+00:00";
SET @@session.time_zone = "+00:00";
SELECT @@global.time_zone;
SELECT TIMEDIFF(NOW(), UTC_TIMESTAMP);
```

# Teresa

## Instalação (Windows/Mac/Linux)

[https://github.com/luizalabs/teresa/](https://github.com/luizalabs/teresa/)

> Dica: Você pode baixar a última versão do teresa pelo Teresinha
Lembrando que é necessário ter o Node 8+ instalado.

```sh
npm i -g teresinha
```

Atualize com

```sh
teresinha update
```

---

## Instalação (MAC)

```sh
brew tap luizalabs/teresa-cli
brew install teresa
```

---

# Testes

Utilizamos JEST como lib para os testes

## Geração de Tokens

Você pode utilizar a lib `jwtg` para gerar tokens válidos para testes em sua api.

```shell
npm i -g jwtg
```

Uso

```shell
jwtg <secret?> <jsonPath?>
```

Exemplo

```shell
jwtg chave_secreta ./test/fake.json
```

> Dica: Dentro da pasta ./test existe um arquivo chamado fakeUser.json, onde você pode utilizar como cobaia para seus testes.

## E2E

![Visual Studio Code](/.repo/test.png)

Você deve estruturar seus testes ordenando seus nomes com números.

# Tricks

## Erros

Em caso de cenário de erro, a stack é retornada no seguinte atributo do cabeçalho de respostas:

```headers
x-error-message
```

# SonarQube

> O SonarQube é uma das mais conhecidas ferramentas para análise estática de código. Ela tem como principal característica permitir a inspeção contínua de qualidade do código, que permite realizar revisões automáticas com análise estática de código, detecção de erros, vulnerabilidades de segurança, dentre outros recursos, visando a melhora contínua da qualidade do código produzido.

Crie seu projeto em <https://sonarqube.luizalabs.com/projects>

![Sonar](/.repo/sonar.png)

Configure o `sonar.projectKey` como em: [sonar-project.properties](./sonar-project.properties)

Não esqueça também de sincronizar a versão do `package.json` juntamente com a chave `sonar.projectVersion`

Assim seu projeto estará configurado.