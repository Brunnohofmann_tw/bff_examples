### Descrição
(Breve resumo do que foi feito, inclua todas as informações relevantes para os avaliadores)

#### Cards
* [USTXXX](https://luizalabs.swiftkanban.com/pui/index.jsp#projectid=127&itemid=USTXXX)

#### Testes
Se não tiver testes, fique à vontade para marcar o checkbox da vergonha
- [ ] Testes implementados
- [ ] ¯\＿(ツ)＿/¯

-------------------------------
#### Lembre-se
- Verificar a versão no [package.json](/package.json)
- https://sonarqube.luizalabs.com/dashboard?id=scaffolding

/assign me
/target_branch stage

@luizalabs/squad-{nome-da-minha-squad}
