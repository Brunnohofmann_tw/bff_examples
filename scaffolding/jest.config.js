module.exports = {
  preset: 'ts-jest',
  bail: true,
  testEnvironment: 'node',
  verbose: true,
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  coveragePathIgnorePatterns: [
    "<rootDir>/node_modules",
    "<rootDir>/scripts",
    "<rootDir>/__tests__",
  ],
  coverageThreshold: {
    global: {
      "branches": 20,
      "functions": 30,
      "lines": 50,
      "statements": 50
    }
  },
  testRegex: "index.test.ts$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  collectCoverage: true,
  setupFilesAfterEnv: ["<rootDir>/hackJest"],
  testResultsProcessor: "jest-sonar-reporter"
};