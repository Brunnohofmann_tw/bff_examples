const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');

const envPath = path.join(__dirname, `/../../../env/.env-${process.env.NODE_ENV || 'development'}`);
if (fs.existsSync(envPath)) {
  dotenv.config({ path: envPath, silent: true });
} else {
  console.error(`ENV NOTFOUND: ${envPath}`);
}

const mysql = {
  database: process.env.MYSQL_DATABASE,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  port: process.env.MYSQL_PORT,
  host: process.env.MYSQL_HOST,
  dialect: 'mysql',
  operatorsAliases: false,
  logging: false,
};

module.exports = {
  development: {
    ...(mysql),
  },
  test: {
    ...(mysql),
  },
  stage: {
    ...(mysql),
  },
  production: {
    ...(mysql),
  },
};
