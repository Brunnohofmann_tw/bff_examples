module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('user', [{
      id: 1,
      name: 'GOKU',
      active: 1,
    }, {
      id: 2,
      name: 'VEGETA',
      active: 1,
    }, {
      id: 3,
      name: 'GOHAN',
      active: 1,
    }, {
      id: 4,
      name: 'PICCOLO',
      active: 1,
    }, {
      id: 5,
      name: 'TRUNKS',
      active: 1,
    }]);
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('user', null, {});
  },
};