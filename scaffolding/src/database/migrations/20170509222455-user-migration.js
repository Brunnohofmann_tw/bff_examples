module.exports = {
  up: (queryInterface, dataTypes) => {
    return queryInterface.createTable('user', {
      id: {
        field: 'id',
        type: dataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        field: 'name',
        type: dataTypes.STRING(200),
        allowNull: false,
      },
      active: {
        field: 'active',
        type: dataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('user');
  },
};
