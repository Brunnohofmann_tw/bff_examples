import config from './config/environment.config';

if (config.plugins.newrelic.enabled === 'true') {
  // tslint:disable-next-line: no-var-requires
  require('newrelic');
}

import './config/server.config';

process.on('uncaughtException', (error: Error) => {
  console.error(`uncaughtException ${error.message}`);
});

process.on('unhandledRejection', (reason: any) => {
  console.error(`unhandledRejection ${reason}`);
});
