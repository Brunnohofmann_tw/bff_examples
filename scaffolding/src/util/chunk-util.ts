export default class ChunkUtil {
  public async chunkApiRequest(method: any, array: any[], param = 'id_in', chunkSize = 20) {
    const chunkedArray = await this.chunkArray(array, chunkSize);
    const promises: any = [];

    chunkedArray.forEach((item: any) => {
      const filter = JSON.parse(`{ "${param}":"${item}"}`);
      promises.push(method(filter));
    });

    const result = (await Promise.all(promises)).map((q: any) => q.data.records);
    const mergeResult = [].concat.apply([], result);
    return mergeResult;
  }

  private async chunkArray(array: any[], chunkSize: number) {
    const chunkedArray: any = [];
    for (let index = 0; index < array.length; index += chunkSize) {
      chunkedArray.push(array.slice(index, index + chunkSize).join(', '));
    }
    return chunkedArray;
  }
}