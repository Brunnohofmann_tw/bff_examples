import ApiRequest, { AxiosOptions } from '@luizalabs/hapi-ts/util/api-request.util';
// import { logger } from '../plugins/burzum.plugin';

export default async (options: AxiosOptions) => {
  // Ativa o burzum nas rotas
  // options.logstash = logger;
  options.logMock = true;
  options.log = true;
  return ApiRequest(options);
};
