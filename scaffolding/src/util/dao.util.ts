export default class DAOUtil {
  orderByColumnSplit(options) {
    const elements = options.column.split('.');
    const column = elements.pop();
    const models = elements;

    return { models, column };
  }

  getModels(model, options) {
    const arr: any[] = [];
    for (let i = 0; i < options.include.length; i++) {
      if (options.include[i].model.name === model) {
        arr.push(options.include[i]);
        break;
      }

      if (options.include[i].include && options.include[i].include[0].model.name === model) {
        return this.getModels(model, options.include[i]);
      }
    }

    return arr;
  }

  orderByBuild(options) {
    const splitedColumns = this.orderByColumnSplit(options);
    const orderBy: any[] = [];
    const orderByModel: any[] = [];

    if (splitedColumns.models.length > 0) {
      splitedColumns.models.forEach((model) => {
        orderByModel.push(this.getModels(model, options)[0]);
      });

      orderByModel.map((model) => {
        orderBy.push(model);
      });
      orderBy.push(splitedColumns.column, options.order);
    } else {
      orderBy.push(splitedColumns.column, options.order);
    }

    return [orderBy];
  }
}