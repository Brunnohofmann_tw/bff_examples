import { RequestToolkit, ResponseToolkit } from '@luizalabs/hapi-ts';
import { CREATED, NO_CONTENT, OK } from 'http-status';
import { UserBusiness } from './user.business';

export default class UserController {

  private userBusiness = new UserBusiness();

  async list({ query }: RequestToolkit, h: ResponseToolkit) {
    try {
      const { count: totalRecords, rows } = await this.userBusiness.findAll(query);
      return h.success(rows, { ...(totalRecords && { totalRecords }) }).code(OK);
    } catch (error) {
      return h.error(error);
    }
  }

  async create({ payload }: RequestToolkit, h: ResponseToolkit) {
    try {
      const response = await this.userBusiness.create(payload);
      return h.success(response).code(CREATED);
    } catch (error) {
      return h.error(error);
    }
  }

  async read({ params }: RequestToolkit, h: ResponseToolkit) {
    try {
      const response = await this.userBusiness.byId(params);
      return h.success(response).code(OK);
    } catch (error) {
      return h.error(error);
    }
  }

  async update({ params, payload }: RequestToolkit, h: ResponseToolkit) {
    try {
      const response = await this.userBusiness.update({ params, payload });
      return h.success(response).code(OK);
    } catch (error) {
      return h.error(error);
    }
  }

  async remove({ params }: RequestToolkit, h: ResponseToolkit) {
    try {
      await this.userBusiness.delete(params);
      return h.success({}).code(NO_CONTENT);
    } catch (error) {
      return h.error(error);
    }
  }
}
