import { Op } from 'sequelize';
import DAOUtil from '../../util/dao.util';
import UserDao from './user.dao';

export class UserBusiness {

  private userDao = new UserDao();
  private daoUtil = new DAOUtil();
  public async findAll(query) {

    const { id, name, name_contains, active, limit, offset, count, column, order } = query;
    const params = {
      limit,
      offset,
      where: {
        active,
        ...(id && { id }),
        ...(name && { name }),
        ...(name_contains && { name: { [Op.like]: `%${name_contains}%` } }),
      },
      order: this.daoUtil.orderByBuild({ column, order }),
    };

    if (count) {
      return this.userDao.findAndCountAll(params);
    }

    return this.userDao.findAll(params);

  }

  public async create(payload) {
    return this.userDao.create(payload);
  }

  public async byId(params) {
    const { id } = params;
    return this.userDao.findOne({ where: { id } });
  }

  public async update(options) {
    const { params: { id }, payload } = options;
    await this.userDao.findOne({ where: { id } });
    await this.userDao.update(payload, { where: { id } });
    return payload;
  }

  public async delete(params) {
    const { id } = params;
    await this.userDao.findOne({ where: { id } });
    return this.userDao.destroy({ where: { id }, force: true });
  }
}
