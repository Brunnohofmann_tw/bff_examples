import { joi as Joi } from '@luizalabs/hapi-ts';

export const list = {
  query: Joi.object({
    offset: Joi
      .number()
      .integer()
      .min(0)
      .default(0),
    limit: Joi
      .number()
      .integer()
      .min(1)
      .default(50)
      .max(50),
    active: Joi
      .boolean()
      .default(true),
    count: Joi
      .boolean()
      .default(false),
    column: Joi
      .string()
      .min(1)
      .max(20)
      .equal(['id', 'name', 'active'])
      .default('id'),
    order: Joi
      .string()
      .equal(['ASC', 'DESC'])
      .uppercase()
      .default('ASC'),
  }),
};

export const create = {
  payload: Joi.object().keys({
    name: Joi
      .string()
      .min(3)
      .max(500)
      .trim()
      .required(),
    active: Joi
      .boolean(),
  }).required(),
};

export const read = {
  params: Joi.object({
    id: Joi
      .number()
      .min(1)
      .required(),
  }),
};

export const update = {
  params: Joi.object({
    id: Joi
      .number()
      .min(1)
      .required(),
  }),
  payload: Joi.object().keys({
    name: Joi
      .string()
      .min(3)
      .max(500)
      .trim()
      .required(),
    active: Joi
      .boolean(),
  }).required(),
};

export const remove = {
  params: Joi.object({
    id: Joi
      .number()
      .min(1)
      .required(),
  }),
};
