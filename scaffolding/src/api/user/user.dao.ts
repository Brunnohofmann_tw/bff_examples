import { dao, Entity } from '../../config/dao.config';
import { SequelizeOptions } from '../../config/database.config';
import { UpdateOptions } from 'sequelize';

const { findAndCountAll, findAll, create, destroy, update, findOne } = dao(Entity.user);

export default class UserDao {
  findAndCountAll(options: SequelizeOptions) {
    return findAndCountAll(options);
  }

  findAll(options: SequelizeOptions) {
    return findAll(options);
  }

  create(model: any, options?: any) {
    return create(model, options);
  }

  update(model: any, options: UpdateOptions) {
    return update(model, options);
  }

  findOne(options: SequelizeOptions) {
    return findOne(options);
  }

  destroy(options: SequelizeOptions) {
    return destroy(options);
  }
}
