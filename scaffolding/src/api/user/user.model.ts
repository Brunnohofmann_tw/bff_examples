import { DataTypes, Sequelize } from 'sequelize';
import { Entity, EntityType } from '../../config/dao.config';
import { SequelizeAttributes } from '../../typings/SequelizeAttributes';

export interface UserAttributes {
  id: number;
  name: string;
  active: boolean;
}

export default (sequelize: Sequelize, dataTypes: DataTypes) => {

  const attributes: SequelizeAttributes<UserAttributes> = {
    id: {
      field: 'id',
      type: dataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      field: 'name',
      type: dataTypes.STRING(11),
      allowNull: false,
    },
    active: {
      field: 'active',
      type: dataTypes.BOOLEAN,
      allowNull: false,
    },
  };
  const model = sequelize.define(Entity.user, attributes, {
    tableName: 'user',
    freezeTableName: true,
    timestamps: false,
  });

  model.associate = (_models: EntityType) => {
  };

  return model;
};
