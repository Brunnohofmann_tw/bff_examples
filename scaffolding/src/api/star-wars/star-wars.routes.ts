import Controller from './star-wars.controller';
import * as Validator from './star-wars.schema';

export default async (server) => {
  const controller = new Controller();
  server.bind(controller);

  server.route([{
    method: 'GET',
    path: '/star-wars/people',
    handler: controller.listPeople,
    config: {
      description: 'List people from star wars',
      tags: ['api'],
      validate: Validator.list,
    },
  }, {
    method: 'GET',
    path: '/star-wars/people/{id}',
    handler: controller.readPeople,
    config: {
      description: 'read people from star wars',
      tags: ['api'],
      validate: Validator.read,
    },
  }]);
};