import { joi as Joi } from '@luizalabs/hapi-ts';

export const list = {
  query: Joi.object({
    page: Joi
      .number()
      .integer()
      .min(1)
      .default(1),
  }),
};
export const read = {
  params: Joi.object({
    id: Joi
      .number()
      .min(1)
      .required(),
  }),
};