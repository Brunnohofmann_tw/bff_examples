import { RequestToolkit, ResponseToolkit } from '@luizalabs/hapi-ts';
import { OK } from 'http-status';
import StarWarsBusiness from './star-wars.business';

export default class StarWarsController {

  private business: StarWarsBusiness = new StarWarsBusiness();

  async listPeople({ query }: RequestToolkit, h: ResponseToolkit) {
    try {
      const { data: { results, ...meta } } = await this.business.listPeople({ query });
      return h.successApi(results, meta).code(OK);
    } catch (error) {
      return h.error(error);
    }
  }

  async readPeople({ params }: RequestToolkit, h: ResponseToolkit) {
    try {
      const { data } = await this.business.readPeople({ params });
      return h.successApi(data).code(OK);
    } catch (error) {
      console.log(error);
      return h.error(error);
    }
  }
}