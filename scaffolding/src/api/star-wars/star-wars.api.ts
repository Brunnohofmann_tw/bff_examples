import ApiRequest from '@luizalabs/hapi-ts/util/api-request.util';
import { Config } from '../../config/server.config';

export default class StarWarsApi {

  listPeople({ page }) {
    return ApiRequest({ baseURL: Config.apis.starWars.baseUrl, url: 'api/people', params: { page }, logMock: true });
  }

  readPeople({ id }) {
    return ApiRequest({ baseURL: Config.apis.starWars.baseUrl, url: `api/people/${id}`, logMock: true });
  }
}