import Controller from './report.controller';
import * as Validator from './report.schema';

export default async (server) => {
  const controller = new Controller();
  server.bind(controller);

  server.route([{
    method: 'POST',
    path: '/report/import',
    handler: controller.import,
    config: {
      description: 'Import a file example',
      tags: ['api'],
      validate: Validator.importFile,
      timeout: {
        server: 60000 * 2,
        socket: 60000 * 5,
      },
      payload: {
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data',
        maxBytes: 90000000,
      },
    },
  }, {
    method: 'GET',
    path: '/report/export/{id}',
    handler: controller.export,
    config: {
      description: 'Export a file',
      tags: ['api'],
      validate: Validator.exportFile,
      timeout: {
        server: 60000 * 2,
        socket: 60000 * 5,
      },
    },
  }]);
};