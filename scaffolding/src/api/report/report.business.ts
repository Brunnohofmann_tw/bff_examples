import { FileType } from '@luizalabs/hapi-ts';
import { validateExtension } from '@luizalabs/hapi-ts/util';
import xlsx from 'node-xlsx';

export default class ReportBusiness {

  async import({ file }) {
    validateExtension(file.hapi.filename, [FileType.xlsx]);

    const jsonData: any[] = [];
    const sheets = xlsx.parse(file._data);
    sheets.forEach((sheet) => {
      sheet.data.forEach((item) => {
        jsonData.push([...item]);
      });
    });

    return jsonData;
  }

  async export({ params }) {
    const { id } = params;
    const data = [['exemplo', 'valor'], [id.toString(), 'OK']];
    const file = xlsx.build([{ name: 'pageA', data }]);
    return { filename: `${id}.xlsx`, file };
  }
}