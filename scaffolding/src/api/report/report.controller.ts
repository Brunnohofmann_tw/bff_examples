import { RequestToolkit, ResponseToolkit } from '@luizalabs/hapi-ts';
import { OK } from 'http-status';
import ReportBusiness from './report.business';

export default class ReportController {

  private reportBusiness = new ReportBusiness();

  async import({ payload: { file } }: RequestToolkit, h: ResponseToolkit) {
    try {
      const response = await this.reportBusiness.import({ file });
      return h.success(response).code(OK);
    } catch (error) {
      return h.error(error);
    }
  }

  async export({ params }: RequestToolkit, h: ResponseToolkit) {
    try {
      const { filename, file } = await this.reportBusiness.export({ params });
      return h.download(file, { filename });
    } catch (error) {
      return h.error(error);
    }
  }
}