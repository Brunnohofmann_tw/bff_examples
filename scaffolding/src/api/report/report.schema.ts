import { joi as Joi } from '@luizalabs/hapi-ts';

export const importFile = {
  payload: Joi.object({
    file: Joi.object().required(),
  }).required(),
};

export const exportFile = {
  params: Joi.object({
    id: Joi
      .number()
      .min(1)
      .required(),
  }),
};