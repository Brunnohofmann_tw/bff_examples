import { addEnv, checkEnvs, initEnv } from '@luizalabs/hapi-ts/util/env-checker.util';
import * as path from 'path';

// tslint:disable-next-line:no-var-requires
const pack = require('../../package.json');

(() => {
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  const NODE_ENV = process.env.NODE_ENV;
  const envPath = path.join(__dirname, `/../../${`/env/.env-${NODE_ENV}`}`);
  initEnv(envPath);
})();

const configs = {
  env: addEnv('NODE_ENV'),
  hapi: {
    appName: addEnv('APP_NAME', 'scaffolding'),
    name: pack.name,
    description: pack.description,
    projectVersion: pack.version,
    READINESS_PROBE_DELAY: 10 * 5 * 1000,
    root: path.normalize(path.join(__dirname, '/../..')),
    routesBaseDir: path.normalize(path.join(__dirname, '/../api')),
    prefixPath: addEnv('PREFIX_PATH', '/v1/scaffolding'),
    routesTimeout: Number(addEnv('ROUTES_TIMEOUT', '1000')),
    host: addEnv('HOST', '0.0.0.0'),
    port: Number(addEnv('PORT', '9000')),
    system: addEnv('SYSTEM', '', false),
    requestLog: { enable: addEnv('HTTP_REQUEST_LOG', 'false') },
    routes: {
      cors: {
        additionalExposedHeaders: ['authorization', 'Access-Control-Allow-Origin', 'x-error-message'],
        credentials: true,
      },
      state: {
        parse: false,
        failAction: 'ignore',
      },
      validate: {
        failAction: (_, __, err) => {
          throw err;
        },
        options: {
          abortEarly: false,
        },
      },
    },
  },
  apis: {
    starWars: {
      baseUrl: addEnv('STAR_WARS_URL', 'https://swapi.co'),
    },
  },
  plugins: {
    newrelic: {
      enabled: addEnv('NEW_RELIC_ENABLED', 'false', false),
      token: addEnv('NEW_RELIC_TOKEN', '', false),
      appName: addEnv('NEW_RELIC_APP_NAME', '', false),
    },
    server: {
      enable: addEnv('X_RESPONSE_SERVER_ENABLE', 'true'),
    },
    burzum: {
      enable: addEnv('BURZUM_ENABLE', '', false),
      token: addEnv('BURZUM_TOKEN', '', false),
      port: addEnv('BURZUM_PORT', '', false),
      host: addEnv('BURZUM_HOST', '', false),
    },
    sentry: {
      enable: addEnv('SENTRY_ENABLE', 'false'),
      dsn: addEnv('SENTRY_DSN', '', false),
    },
    login: {
      enable: addEnv('LOGIN_ENABLE', 'false'),
      frontSecret: addEnv('FRONT_SECRET', 'stubJWT'),
      url: addEnv('EMPLOYEES_URL', '', false),
      token: addEnv('EMPLOYEES_TOKEN', '', false),
      systemCode: addEnv('EMPLOYEES_SITE_CODE', '', false),
      expiresIn: addEnv('LOGIN_EXPIRES_IN', '1h'),
      refreshToken: addEnv('LOGIN_REFRESH_TOKEN', 'true') === 'true',
    },
    version: {
      enable: addEnv('VERSION_ENABLE', 'true'),
    },
    replyDecorator: {
      enable: addEnv('REPLY_DECORATOR_ENABLE', 'true', false),
    },
    healtCheck: {
      enable: addEnv('HEALTH_CHECK_ENABLE', 'true'),
    },
    auth: {
      enable: addEnv('AUTH_ENABLE', 'true'),
      secret: addEnv('SECRET', 'stubJWT'),
    },
    documentation: {
      enable: addEnv('DOC_ENABLE', 'true'),
    },
    blip: {
      enable: addEnv('BLIP_ENABLE', 'true'),
      showAuth: addEnv('BLIP_SHOW_AUTH', 'true') === 'true',
    },
    notFound: {
      enable: addEnv('NOT_FOUND_ENABLE', 'true'),
    },
    responseTime: {
      enable: addEnv('RESPONSE_TIME_ENABLE', 'true'),
    },
  },
  mysql: {
    username: addEnv('MYSQL_USER'),
    password: addEnv('MYSQL_PASSWORD', '', false),
    database: addEnv('MYSQL_DATABASE'),
    port: Number(addEnv('MYSQL_PORT')),
    host: addEnv('MYSQL_HOST'),
    operatorsAliases: false,
    pool: {
      min: Number(addEnv('MYSQL_POOL_MIN', '1')),
      max: Number(addEnv('MYSQL_POOL_MAX', '5')),
      idle: Number(addEnv('MYSQL_POOL_IDLE', '10000')),
    },
    replication: {
      write: { ...(process.env.MYSQL_WRITE_HOST ? { host: addEnv('MYSQL_WRITE_HOST') } : {}) },
      read: [
        ...[process.env.MYSQL_READ_HOST_1 ? { host: addEnv('MYSQL_READ_HOST_1') } : {}],
      ],
    },
    logging: addEnv('LOG_DB', 'true') === 'true' ? console.log : false,
    dialect: 'mysql',
    benchmark: addEnv('MYSQL_BENCHMARK', 'true') === 'true',
  },
};

checkEnvs();
export default configs;
