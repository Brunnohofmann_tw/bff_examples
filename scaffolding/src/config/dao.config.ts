import { isAlreadyExistsError, isForeignKeyConstraintError, throwIfEmpty } from '@luizalabs/hapi-ts/business/error.business';
import { camelCase } from 'lodash';
import { BulkCreateOptions, CreateOptions, DestroyOptions, FindOptions, QueryOptions, UpdateOptions } from 'sequelize';
import { connection } from './database.config';
import { Entity, EntityTranslated } from './entity.config';
export * from './entity.config';

export type EntityType = {
  [P in Entity]: any | string;
};

export function dao(key: Entity) {
  const dao = connection().models[Entity[key]];
  return {
    findOne: async (options: {}, throwIfEmptyEnable = true) => {
      const item = (await dao.findOne(options));
      const dataValues = item ? item.toJSON() : null;

      if (throwIfEmptyEnable) {
        throwIfEmpty(EntityTranslated[key] || Entity[key])(dataValues);
      }
      return dataValues;
    },
    findAll: async (options?: FindOptions<any>, throwIfEmptyEnable = true) => {
      const result = await dao.findAll(options);
      const dataValues = result.map((e) => {
        return e.toJSON();
      });
      if (throwIfEmptyEnable) {
        throwIfEmpty(EntityTranslated[key] || Entity[key])(dataValues);
      }
      return { rows: dataValues, count: undefined };
    },
    findAndCountAll: async (options?: FindOptions<any>, throwIfEmptyEnable = true) => {
      const result = await dao.findAndCountAll(options);
      const dataValues = {
        rows: result.rows.map((e) => {
          return e.toJSON();
        }), count: result.count
      };

      if (throwIfEmptyEnable) {
        throwIfEmpty(EntityTranslated[key] || Entity[key])(dataValues.rows);
      }
      return dataValues;
    },
    create: async (values: {}, options?: CreateOptions) => {
      try {
        return (await dao.create(values, options)).toJSON();
      } catch (error) {
        isForeignKeyConstraintError(error, camelCase(error.table || EntityTranslated[key] || Entity[key]));
        isAlreadyExistsError(error, EntityTranslated[key] || Entity[key]);
        throw error;
      }
    },
    bulkCreate: async (values: [], options?: BulkCreateOptions) => {
      try {
        return (await dao.bulkCreate(values, options));
      } catch (error) {
        isForeignKeyConstraintError(error, camelCase(error.table || EntityTranslated[key] || Entity[key]));
        isAlreadyExistsError(error, EntityTranslated[key] || Entity[key]);
        throw error;
      }
    },
    update: async (values: {}, options: UpdateOptions) => {
      try {
        return await dao.update(values, options);
      } catch (error) {
        isForeignKeyConstraintError(error, camelCase(error.table || EntityTranslated[key] || Entity[key]));
        isAlreadyExistsError(error, EntityTranslated[key] || Entity[key]);
        throw error;
      }
    },
    destroy: async (options: DestroyOptions) => {
      return dao.destroy(options);
    }
  };
}

export function query(params: { query: string, values: any[] }, options?: QueryOptions) {
  return connection().query(params, options);
}

export function model(key: Entity) {
  return connection().models[Entity[key]];
}