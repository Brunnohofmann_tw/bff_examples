import * as KlawSync from 'klaw-sync';
import * as Sequelize from 'sequelize';
import Config from './environment.config';

const createDBConnection = () => {
  const { replication, pool, ...mysql } = Config.mysql;
  if (replication && replication.read) {
    const [readLength, writeLength] = [replication.read.length, replication.write ? 1 : 0];
    if (readLength > 0 && writeLength === 0 ||
      readLength === 0 && writeLength > 0) {
      throw new Error('To enable the use of replication you must inform a database write and at least one database read');
    }
  }
  return new Sequelize(mysql);
};

let sequelizeConnection = createDBConnection();
sequelizeConnection.authenticate().catch((err) => {
  console.error('[Sequelize] Unable to connect to the database:', err.message);
});

const db = {};
const models: string[] = [];
KlawSync(Config.hapi.routesBaseDir)
  .filter(item => /([a-zA-Z0-9\s_\\.\-\(\):])+(model)+(.js|.ts)$/.test(item.path))
  .forEach((file) => {
    const model = sequelizeConnection.import(file.path);
    db[model.name] = model;
    models.push(model.name);
  });

console.log(`[Sequelize] Models loaded: ${models.join(' - ')}`);

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

const connection = () => {
  if (!sequelizeConnection) {
    sequelizeConnection = createDBConnection();
  }
  return sequelizeConnection;
};
export interface SequelizeIncludeOptions extends Sequelize.IncludeOptions {
  limit?: number;
}

export interface SequelizeOptions {
  distinct?: boolean;
  attributes?: any[];
  group?: any[];
  where?: any;
  limit?: number;
  offset?: number;
  include?: SequelizeIncludeOptions[];
  force?: boolean;
  order?: any;
  paranoid?: boolean;
  all?: boolean | string;
  nested?: boolean;
  separate?: boolean;
}

export { connection };