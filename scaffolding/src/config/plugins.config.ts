import {
  AuthPlugin,
  Blipp,
  DocumentationPlugin,
  HealthCheckPlugin,
  LoginPlugin,
  LogstashPlugin,
  NotFoundPlugin,
  ReplyDecoratorPlugin,
  ResponseTime,
  RoutesPlugin,
  SentryPlugin,
  ServerPlugin,
  VersionPlugin,
} from '@luizalabs/hapi-ts/plugin';
import { AuthOptions } from '@luizalabs/hapi-ts/plugin/auth.plugin';
import { DocumentationOptions } from '@luizalabs/hapi-ts/plugin/documentation.plugin';
import { HealthCheckOptions } from '@luizalabs/hapi-ts/plugin/health-check.plugin';
import { LoginOptions } from '@luizalabs/hapi-ts/plugin/login.plugin';
import { LogstashOptions } from '@luizalabs/hapi-ts/plugin/logstash.plugin';
import { RoutesOptions } from '@luizalabs/hapi-ts/plugin/routes.plugin';
import { SentryOptions } from '@luizalabs/hapi-ts/plugin/sentry.plugin';
import { VersionOptions } from '@luizalabs/hapi-ts/plugin/version.plugin';
import { connection } from './database.config';
import Config from './environment.config';

const isTrue = (value) => {
  return value === 'true';
};

export const plugins = [
  {
    plugin: RoutesPlugin,
    options: <RoutesOptions> {
      routesBaseDir: Config.hapi.routesBaseDir,
      prefix: Config.hapi.prefixPath,
    },
  },
  ...(isTrue(Config.plugins.replyDecorator.enable) ? [{
    plugin: ReplyDecoratorPlugin,
  }] : []),
  ...(isTrue(Config.plugins.server.enable) ? [{
    plugin: ServerPlugin,
  }] : []),
  ...(isTrue(Config.plugins.version.enable) ? [{
    plugin: VersionPlugin,
    options: <VersionOptions> {
      versionApi: Config.hapi.projectVersion,
    },
  }] : []),
  ...(isTrue(Config.plugins.notFound.enable) ? [{
    plugin: NotFoundPlugin,
  }] : []),
  ...(isTrue(Config.plugins.responseTime.enable) ? [{
    plugin: ResponseTime,
  }] : []),
  ...(isTrue(Config.plugins.login.enable) ? [{
    plugin: LoginPlugin,
    options: <LoginOptions> {
      prefixPath: Config.hapi.prefixPath,
      expiresIn: Config.plugins.login.expiresIn,
      secret: Config.plugins.auth.secret,
      frontSecret: Config.plugins.login.frontSecret,
      token: Config.plugins.login.token,
      systemCode: Config.plugins.login.systemCode,
      url: Config.plugins.login.url,
      refreshToken: Config.plugins.login.refreshToken,
    },
  }] : []),
  ...(isTrue(Config.plugins.healtCheck.enable) ? [{
    plugin: HealthCheckPlugin,
    options: <HealthCheckOptions> {
      prefixPath: Config.hapi.prefixPath,
      sequelize: connection(),
    },
  }] : []),
  ...(isTrue(Config.plugins.documentation.enable) ? [{
    plugin: DocumentationPlugin,
    options: <DocumentationOptions> {
      versionApi: Config.hapi.projectVersion,
      title: Config.hapi.name,
      basePath: Config.hapi.prefixPath,
    },
  }] : []),
  ...(isTrue(Config.plugins.auth.enable) ? [{
    plugin: AuthPlugin,
    options: <AuthOptions> {
      key: Config.plugins.auth.secret,
    },
  }] : []),
  ...(isTrue(Config.plugins.sentry.enable) ? [{
    plugin: SentryPlugin,
    options: <SentryOptions> {
      dsn: Config.plugins.sentry.dsn,
      release: Config.hapi.projectVersion,
      environment: Config.env,
    },
  }] : []),
  ...(isTrue(Config.plugins.burzum.enable) ? [{
    plugin: LogstashPlugin,
    options: <LogstashOptions> {
      host: Config.plugins.burzum.host,
      port: Config.plugins.burzum.port,
      bztoken: Config.plugins.burzum.token,
      env: Config.env,
      versionApi: Config.hapi.projectVersion,
      appName: Config.hapi.appName,
    },
  }] : []),
  ...(isTrue(Config.plugins.blip.enable) ? [{
    plugin: Blipp, options: { showAuth: Config.plugins.blip.showAuth },
  }] : []),
];

if (isTrue(Config.plugins.newrelic.enabled)) {
  // tslint:disable-next-line: no-var-requires
  require('newrelic');
}

export async function sigterm(server) {
  process.on('SIGTERM', () => {
    console.info('Got SIGTERM. Graceful shutdown start', new Date().toISOString());
    setTimeout(greacefulStop, Config.hapi.READINESS_PROBE_DELAY);
  });

  async function greacefulStop() {
    console.info('server is shutting down...', new Date().toISOString());
    try {
      await server.stop({
        timeout: 10000,
      });
      console.info('Successful graceful shutdown', new Date().toISOString());
      process.exit(0);
    } catch (error) {
      console.error('Error happened during graceful shutdown', error);
      process.exit(1);
    }
  }
}