import * as DnsCache from 'dnscache';
import { hapi as Hapi } from '@luizalabs/hapi-ts';
import Config from './environment.config';
import { plugins, sigterm } from './plugins.config';

declare var process;

const { host, port, routes, requestLog } = Config.hapi;

const server = new Hapi.Server(<any> {
  host,
  port,
  routes,
});

(async () => {
  try {

    if (requestLog.enable === 'true') {
      server.events.on('response', (request: any) => {
        const queryParams = Object.keys(request.query).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(request.query[k])}`).join('&');
        console.log(`--- Request ${request.info.remoteAddress} - ${request.info.referrer} ---`);
        console.log(`${request.method.toUpperCase()} [${request.response.statusCode}]: ${request.url.pathname}?${queryParams}`);
        if (request.method === 'post' || request.method === 'put') {
          console.log(`Payload:\n${JSON.stringify(request.payload)}`);
        }
        if (request.response.source) {
          console.log(`Response:\n${JSON.stringify(request.response.source)}`);
        }
      });
    }

    sigterm(server);

    DnsCache({ enable: true, ttl: 300, cachesize: 1000 });

    await server.register(plugins);
    if (process.env.NODE_ENV !== 'test') {
      await server.start();
      console.log('\x1b[36m%s\x1b[0m', `*** [${Config.hapi.name}] - v${Config.hapi.projectVersion} ***`);
    }
    return server;
  } catch (error) {
    console.error('Server failed to start 8(');
    console.error(error.message);
    console.error(error.stack);
    return process.exit(0);
  }
})();

export { Config };
export default server;