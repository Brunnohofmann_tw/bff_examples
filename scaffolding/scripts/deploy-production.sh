wget https://github.com/luizalabs/teresa/releases/download/v0.23.0/teresa-linux-amd64

mv ./teresa-linux-amd64 teresa

chmod +x ./teresa

./teresa version

./teresa config set-cluster production --server teresa.magazineluiza.com.br --tls --current

./teresa config use-cluster production

echo $TERESA_PASSWORD | ./teresa login --user $TERESA_USER

./teresa app list

rm package-lock.json

./teresa deploy create . --app scaffolding --no-input