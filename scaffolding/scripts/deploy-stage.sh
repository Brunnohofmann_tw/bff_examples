echo 'Starting deploy...'
wget https://github.com/luizalabs/teresa/releases/download/v0.28.0/teresa-linux-amd64

mv ./teresa-linux-amd64 teresa

chmod +x ./teresa

./teresa version

./teresa config set-cluster new-staging --server teresa-staging-us-east-1.magazineluiza.com.br --tls

./teresa config use-cluster new-staging

echo $TERESA_PASSWORD | ./teresa login --user $TERESA_USER

./teresa app list

rm -rf package-lock.json

./teresa deploy create . --app scaffolding --no-input