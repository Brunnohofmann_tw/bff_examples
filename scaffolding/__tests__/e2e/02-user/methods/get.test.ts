import { route, TestRouteOptions } from '../../../commons';

it('[GET] return 200 when search specified id', async () => {
  const params: TestRouteOptions = {
    method: 'GET',
    url: 'users/1',
  };
  const { payload, statusCode } = await route(params);
  expect(statusCode).toBe(200);
  expect(payload).toHaveProperty('records');
  expect(payload.records).toHaveLength(1);
  expect(payload.records[0]).toEqual({ id: 1, name: 'GOKU', active: true });
});