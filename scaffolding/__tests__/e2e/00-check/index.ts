describe('Jest Is OK?', () => {
  it('Check if Jest tests are OK', async () => {
    const sub = (a, b) => {
      return a - b;
    };
    expect(sub(5, 1)).toBe(4);
  });
});