import * as nock from 'nock';
import { luke } from './mock';

export const peopleLuke = () =>
  nock('https://swapi.co')
    .get('/api/people/1')
    .query(true)
    .reply(200, luke);