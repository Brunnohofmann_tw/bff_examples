import { route, TestRouteOptions } from '../../../commons';
import { luke } from '../mock';
import * as nock from '../nock';

it('[GET] return 200 when search specified id', async () => {

  const params: TestRouteOptions = {
    method: 'GET',
    url: 'star-wars/people/1',
  };

  nock.peopleLuke();

  const { payload, statusCode } = await route(params);
  expect(statusCode).toBe(200);
  expect(payload).toHaveProperty('records');
  expect(payload.records).toHaveLength(1);
  expect(payload.records[0]).toEqual(luke);
});