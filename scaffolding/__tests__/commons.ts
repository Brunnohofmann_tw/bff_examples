import * as fs from 'fs';
import * as JWT from 'jsonwebtoken';
import * as nock from 'nock';
import * as path from 'path';
import * as qs from 'querystring';
import config from '../src/config/environment.config';
import Server from '../src/config/server.config';

import { hapi as Hapi } from '@luizalabs/hapi-ts';

// tslint:disable-next-line:no-var-requires
const fakeJson = require('./fake.json');
export const token = JWT.sign(fakeJson, config.plugins.auth.secret);

interface Route { statusCode: number; headers: any; payload?: any; }
export interface TestRouteOptions {
  url: string;
  method: 'PUT' | 'GET' | 'POST' | 'DELETE';
  query?: any;
  payload?: any;
  headers?: any;
  auth?: boolean;
}

const route = async (parameters: TestRouteOptions): Promise<Route> => {
  const { url, method, query, payload, headers, auth } = parameters;

  const stringify = query ? `?${qs.stringify(query)}` : '';

  const options: Hapi.ServerInjectOptions = {
    method,
    url: `${config.hapi.prefixPath}/${url}${stringify}`,
    headers: {
      ...((auth === undefined || auth) && { authorization: token }),
      ...headers,
    },
    ...(payload && { payload }),
  };

  const { result, 'headers': responseHeaders, statusCode } = await Server.inject(options);
  return { payload: result, headers: responseHeaders, statusCode };
};

export const loadFiles = (currentDir, sequence) => {
  const folder = path.resolve(`${currentDir}/methods/`);
  const files = fs.readdirSync(folder);

  const modulePath = (name) => {
    return `${currentDir}/methods/${name}`;
  };
  const existModulePath = (name) => {
    return fs.existsSync(`${currentDir}/methods/${name}`);
  };

  for (let i = 0; i < sequence.length; i++) {
    const item = `${sequence[i]}.test`;
    files.filter((file) => {
      if (file.indexOf(item) >= 0) {
        if (existModulePath(file)) {
          require(modulePath(file));
        } else {
          throw new Error(`File not found: ${file}`);
        }
      }
    });
  }
};

export const requireMethods = (description, currentDir, sequence = ['post', 'get', 'put', 'delete']) => {
  describe(description, () => {

    beforeAll(async () => {
      jest.useFakeTimers();
      Date.now = jest.fn(() => 1503187200000);
      nock.disableNetConnect();
      nock.enableNetConnect(/^(127\.0\.0\.1|localhost)/);
      nock.cleanAll();
      jest.resetAllMocks();
    });

    beforeEach(async () => {
      nock.cleanAll();
      jest.resetModules();
    });

    afterEach(async () => {
      nock.cleanAll();

    });

    loadFiles(currentDir, sequence);
  });
};

export { route };